#!/bin/bash
in=headwords-6.tsv
quiz=index.adoc
tsv="quiz.tsv"
theme=mandenkan

source ./date_params.sh

for x in shuf; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

cp "./$theme.css" node_modules/reveal.js/css/theme/
[[ -d ./node_modules/reveal.js/css/theme/images ]] || mkdir ./node_modules/reveal.js/css/theme/images/
cp ./images/mandenkan.png node_modules/reveal.js/css/theme/images/
printf ":revealjs_theme: $theme\n" 

printf "= Dioulakan kalan lon o lon !\n" > $quiz
printf ":revealjs_theme: $theme\n" >> $quiz 
printf ":title-slide-transition: zoom\n" >> $quiz
printf "//:revealjs_slidenumber: true\n" >> $quiz
printf ":revealjs_controlsTutorial: true\n" >> $quiz
printf ":revealjs_navigationMode: linear\n" >> $quiz
printf "//:revealjs_shuffle: false\n" >> $quiz
printf ":icons: font\n" >> $quiz
printf "include::locale/attributes.adoc[]\n\n" >> $quiz

printf "Trouvez la traduction exacte des phrases dioula suivantes vers le français. +\n" >> $quiz
printf "La flêche droite affiche d'abord un indice, et ensuite la traduction en français. \n" >> $quiz
printf "Il y a du nouveau chaque jour, alors revenez demain! \n\n" >> $quiz
printf "* I ni baara ! \n\n" >> $quiz

printf "== Bi ye Lon jumɛn ye ?\n" >> $quiz
printf "[step=1]\n" >> $quiz
printf "bi ye [.step]#%s# ye,\n\n" "$dow" >> $quiz
echo -e '[%step]\n' >> $quiz
echo -e "[%step]#$month#, tere [.step]#$day#,\n\n" >> $quiz
echo -e "[%step]\n" >> $quiz
printf "saan [.step]#waga fila ani mugan ni kelen kɔnɔ#\n\n" >> $quiz


shuf -n 5 $in | awk -v quiz=$quiz -F"\t" '{ print "== ", $2, "\n" >> quiz}
{ print "[TIP,step=1]" >> quiz } 
{ print $1, "\n" >> quiz}
{ print "=== ", $3, "\n\n" >> quiz}'

printf "== Félicitations ! !\n\n" >> $quiz
printf "* Mais n'oubliez pas de revenir demain pour un nouveau jeu de phrases.\n" >> $quiz
printf "* Et si vous avez des questions sur le Jula, le Bambara, ou le Mandenkan, adressez-vous à contact@mandenkan.com.\n" >> $quiz
printf "* Cours de langue Jula, Bambara et Mandenkan disponibles.  Site web: https://www.mandenkan.com\n" >> $quiz

npx asciidoctor-revealjs $quiz 
