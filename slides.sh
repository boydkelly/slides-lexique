#!/bin/bash
in=headwords-6.tsv
dest=./
quiz=index.adoc
tsv="quiz.tsv"
theme=mandenkan

source ./date_params.sh

for x in shuf; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

cp "./theme/$theme.css" node_modules/reveal.js/css/theme/
[[ -d ./node_modules/reveal.js/css/theme/images ]] || mkdir ./node_modules/reveal.js/css/theme/images/
cp ./images/mandenkan.png node_modules/reveal.js/css/theme/images/
cp -r "./theme/fonts/noto-sans" node_modules/reveal.js/css/theme/fonts/

cat <<EOF > $quiz
= Dioulakan kalan lon o lon !"
:revealjs_theme: $theme"
:title-slide-transition: zoom"
//:revealjs_slidenumber: true"
:revealjs_controlsTutorial: true"
:revealjs_navigationMode: linear"
//:revealjs_shuffle: false"
:icons: font"

Trouvez la traduction exacte des phrases dioula suivantes vers le français. +\n" >> $quiz
La flêche droite affiche d'abord un indice, et ensuite la traduction en français. \n" >> $quiz
Il y a du nouveau chaque jour, alors revenez demain! \n\n" >> $quiz
* I ni baara !**

EOF

printf "== Bi ye Lon jumɛn ye ?\n" >> $quiz
printf "[step=1]\n" >> $quiz
printf "bi ye [.step]#%s# ye,\n\n" "$dow" >> $quiz
echo -e '[%step]\n' >> $quiz
echo -e "[%step]#$month#, tere [.step]#$day#,\n\n" >> $quiz
echo -e "[%step]\n" >> $quiz
printf "saan [.step]#waga fila ani mugan ni kelen kɔnɔ#\n\n" >> $quiz


shuf -n 5 $in | awk -v quiz=$quiz -F"\t" '{ print "== ", $2, "\n" >> quiz}
{ print "[TIP,step=1]" >> quiz } 
{ print $1, "\n" >> quiz}
{ print "=== ", $3, "\n\n" >> quiz}'

printf "== Félicitations ! !\n\n" >> $quiz
printf "* Mais n'oubliez pas de revenir demain pour un nouveau jeu de phrases.\n" >> $quiz
printf "* Et si vous avez des questions sur le Jula, le Bambara, ou le Mandenkan, adressez-vous à contact@mandenkan.com.\n" >> $quiz
printf "* Cours de langue Jula, Bambara et Mandenkan disponibles.  Site web: https://www.mandenkan.com\n" >> $quiz

npx asciidoctor-revealjs -v -D $dest $quiz 
