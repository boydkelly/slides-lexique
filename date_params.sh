#!/usr/bin/bash
dows=( "tenenlɔn" "taratalɔn" "arabalɔn" "alamisalɔn" "jumalɔn" "sibirilɔn" "karilɔn" )
months=( 
  "zanviekalo" 
  "feburukalo" 
  "marsikalo" 
  "avirilikalo" 
  "mekalo" 
  "zuenkalo" 
  "zuwekalo" 
  "utikalo" 
  "setanburukalo" 
  "novemburukalo" 
  "desemburukalo" 
)

days=(
  "kelen"
  "fila"
  "saba"
  "naani"
  "looru"
  "wɔrɔ"
  "wɔlɔnfila"
  "seegi"
  "kɔnɔtɔn"
  "tan"
  "tan ni kelen"
  "tan ni fila"
  "tan ni saba"
  "tan ni naani"
  "tan ni looru"
  "tan ni wɔrɔ"
  "tan ni wɔlɔnfila"
  "tan ni seegi"
  "tan ni kɔnɔtɔn"
  "mugan"
  "mugan ni kelen"
  "mugan ni fila"
  "mugan ni saba"
  "mugan ni naani"
  "mugan ni looru"
  "mugan ni wɔrɔ"
  "mugan ni wɔlɔnfila"
  "mugan ni seegi"
  "mugan ni kɔnɔtɔn"
  "bi saba"
  "bi saba ni kelen"
  )

day=${days[$(date +%d)-1]}
dow=${dows[$(date +%w)-1]}
month=${months[$(date +%m)-1]}


